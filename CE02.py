import os


def menu():
    print("")
    print("1) Registrar alumnos.")
    print("2) Mostrar listado.")
    print("3) Buscar alumno por dni.")
    print("4) Modificar alumno por DNI.")
    print("5) Eliminar alumno.")
    print("6) Mostrar alumnos con nota mayor al promedio.")
    print("7) Salir.")
    print("")

    

def verificarOpcion():

    while True:
        try:
            print()
            opcion=int(input("Ingrese una opcion:"))
            if 1<=opcion<=7:
                return opcion
            else:
                print("INGRESO NO VALIDO...")
        except:
            print("INGRESO NO VALIDO...")


def verificarDni():
    
    while True:
        try:
            valor=int(input("DNI: "))
            if valor>=0:
                return valor
            else:
                print("ERROR...")
        except:
            print("ERROR...")
    


def verificarNota():

    while True:
        try:
            valor=int(input("Nota: "))
            if 0<=valor<=10:
                return valor
            else:
                print("ERROR...")
        except:
            print("ERROR...")

def registrarAlumnos():
    dni=1

    print("Registro de alumnos")
    print("")
    print("(Para finalizar ingrese un DNI igual a 0)")

    while dni!=0:

        dni=verificarDni()
        if dni!=0:

            nombre=input("Nombre: ")
            nota=verificarNota()

            listado[dni]=[nombre,nota]
    return listado


        

        
def mostrarListado(listado):
    for clave,valor in listado.items():
        print(clave,valor)
            

def buscarAlumno(listado):
    busqueda=verificarDni()

    for clave,valor in listado.items():
        if clave==busqueda:
            print(clave,valor)


def modificarAlumno():



    dni=verificarDni()
    band=False

    for clave,valor in listado.items():
        if clave==dni:
            band=True
            nombre=input("Nombre: ")
            nota=verificarNota()
            listado[clave]=[nombre,nota]
            

    if band==False:
        print("DNI no registrado...")
    
    return listado
    
    

def eliminarAlumno():

    dni=verificarDni()

    for clave,valor in listado.items():
        
        if clave==dni:
            listado.pop(dni)
            print("Alumno borrado correctamente")
            break
    
    return listado


#Principal---------------------------

os.system('cls')
listado={23:["Lopez",3],43:["Diaz",4],28:["Martinez",6],74:["Gutierrez",8]}

opcion=0

while opcion!=7:
    menu()

    opcion=verificarOpcion()

    if opcion==1:
        listado=registrarAlumnos()
    elif opcion==2:
        mostrarListado(listado)
    elif opcion==3:
        buscarAlumno(listado)
    elif opcion==4:
        lisado=modificarAlumno()
    elif opcion==5:
        listado=eliminarAlumno()
